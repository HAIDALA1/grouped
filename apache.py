from fabric import Connection


class Apache:

    def __init__(self, host, username) -> None:
        self.connection = Connection(host=host, user=username)

    def installer(self) -> None:
        try:
           self.connection.run('sudo apt install apache2', hide=True)
        except:
            raise Exception("Echec apt install")

    def uninstall(self) -> None:
        try:
           self.connection.run('sudo apt remove apache2*', hide=True)
        except:
            raise Exception("Echec apt remove apache2*")


